(ns sock.http-kit
  (:require
    [clojure.core.async :refer [<! >! put! close! chan buffer sliding-buffer go-loop]]
    [clojure.edn :as edn]
    [org.httpkit.server :refer :all]))


(defn ws-handler [accept-chan & {:keys [read-chan-fn write-chan-fn meta-chan-fn wire-spec]
                                 :or   {read-chan-fn  #(chan (sliding-buffer 1024))
                                        write-chan-fn #(chan (sliding-buffer 1024))
                                        meta-chan-fn  #(chan (buffer 64))
                                        wire-spec {:name    "application/edn" ;TODO check encoding and throw error on mismatch
                                                   :encoder pr-str
                                                   :decoder edn/read-string}}}]
  (fn [request]
    (when (:websocket? request)
      (with-channel
        request channel
        (when (websocket? channel)
          (let [read-chan (read-chan-fn)
                write-chan (write-chan-fn)
                meta-chan (meta-chan-fn)]
            (on-close channel (fn [status]
                                (put! meta-chan {:event  :close
                                                 :status status})
                                (close! read-chan)
                                (close! write-chan)
                                (close! meta-chan)))
            (on-receive channel (fn [data]
                                  (put! read-chan ((:decoder wire-spec) data))))
            (go-loop []
              (when (open? channel)
                (if-let [data (<! write-chan)]
                  (do (send! channel ((:encoder wire-spec) data))
                      (recur))
                  (close channel))))
            (put! accept-chan {:read-chan  read-chan
                               :write-chan write-chan
                               :meta-chan  meta-chan})
            (put! meta-chan {:event :open})))))))
