(ns sock.cljs
  (:require [cljs.core.async :refer [chan buffer sliding-buffer put! >! <! close! map< filter<]]
            cljs.core.async.impl.protocols
            [cljs.reader :as reader])
  (:require-macros [cljs.core.async.macros :refer [go go-loop]]))

(def readystate-open 1)

(defn ws
  "Connects to the given adress via a webSocket and
  returns a set of channels to communicate with the socket.

  This collection is of the form
  `{:in dropping-chan :out dropping-chan :log dropping-chan}`.

  Closing the `:out` channel closes the websocket."
  [address & {:keys [read-chan write-chan meta-chan wire-spec]
              :or {read-chan  (chan (sliding-buffer 1024))
                   write-chan (chan (sliding-buffer 1024))
                   meta-chan  (chan (buffer 64))
                   wire-spec {:name    "application/edn"
                              :encoder pr-str
                              :decoder reader/read-string}}}]
  (let [socket (js/WebSocket. address)]
    (doto socket
      (aset "onopen" (fn [_]
                       (put! meta-chan {:event :open})
                       (go-loop []
                                (when (= (.-readyState socket) readystate-open)
                                  (if-let [msg (<! write-chan)]
                                    (do (.send socket ((:encoder wire-spec) msg))
                                        (recur))
                                    (.close socket))))))
      (aset "onmessage" (fn [event]
                          (let [msg (.-data event)]
                            (put! read-chan ((:decoder wire-spec) msg)))))
      (aset "onerror"   (fn [event]
                          (let [error (.-data event)]
                            (put! meta-chan {:event  :error
                                             :status error}))))
      (aset "onclose" (fn [event]
                        (let [code (.-code event)
                              reason (.-reason event)]
                          (put! meta-chan {:event  :close
                                           :code   code
                                           :reason reason})
                          (close! read-chan)
                          (close! write-chan)
                          (close! meta-chan)))))
    {:meta-chan meta-chan
     :read-chan read-chan
     :write-chan write-chan}))