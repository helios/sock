(ns sock.http-kit.util
  (:require
    [clojure.core.async :as a]
    [sock.http-kit :as sock]))

(defn ws-broadcast-handler
  "Creates a hadnler that echoes and broadcast all messages send to it to all other connections."
  []
  (let [broadcast-chan (a/chan)
        broadcast-mult (a/mult broadcast-chan)
        broadcast-mix (a/mix broadcast-chan)
        accepts-chan (a/chan)]
    (a/go-loop
      [{:keys [read-chan write-chan meta-chan] :as connection} (a/<! accepts-chan)]
      (when connection
        (a/admix broadcast-mix read-chan)
        (a/tap broadcast-mult write-chan)
        (a/go-loop [meta-msg (a/<! meta-chan)]
          (cond
            (= meta-msg nil) nil
            (= (:event meta-msg) :close)
            (do
              (a/unmix broadcast-mix read-chan)
              (a/untap broadcast-mult write-chan))
            :else (recur (a/<! meta-chan))))
        (recur (a/<! accepts-chan))))
    (sock/ws-handler accepts-chan)))

(def prepared-ws-broadcast-handler (ws-broadcast-handler))  ;Use for ring config, like in figwheel.