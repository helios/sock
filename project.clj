(defproject is.pum/sock "0.1.0-SNAPSHOT"
  :description "A thin core.async wrapper for websockets in clojurescript and http-kit."
  :url "https://gitlab.com/pumis/sock"
  :license {:name "MIT"
            :url "http://ticking.mit-license.org"}
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [org.clojure/clojurescript "0.0-3308"]
                 [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                 [http-kit "2.1.19"]])
