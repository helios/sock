# sock
A thin core.async wrapper for websockets in clojurescript and http-kit.

## installation
Add the following dependency to your project.clj file:

[![Clojars Project](http://clojars.org/is.pum/sock/latest-version.svg)](http://clojars.org/is.pum/sock)

## Philosophy

This is really as simple as it gets, no custom channels no automatic reconnects and dropping by default.

The bitter truth is that networks loose things (be it at the point a connection is dropped and can't be reconnected),
so you should take care of this in your application protocol, by for example using idempotent operations.

I f you want something more feature rich you should take a look at [sente](https://github.com/ptaoussanis/sente) or [chord](https://github.com/james-henderson/chord).